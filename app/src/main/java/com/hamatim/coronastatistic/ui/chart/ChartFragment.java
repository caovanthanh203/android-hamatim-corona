package com.hamatim.coronastatistic.ui.chart;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hamatim.coronastatistic.MainActivity;
import com.hamatim.coronastatistic.R;
import com.hamatim.coronastatistic.model._chart.ChartReport;
import com.hamatim.coronastatistic.ui.OnCustomClickListener;
import com.hamatim.coronastatistic.ui.map.MapFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ChartFragment extends Fragment implements View.OnClickListener, OnCustomClickListener {

    private LineChart chart;
    private RelativeLayout relativeLayout;
    private ChartViewModel chartViewModel;
    private ImageButton customMenu;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        chartViewModel = ViewModelProviders.of(this).get(ChartViewModel.class);
        View root = inflater.inflate(R.layout.fragment_chart, container, false);
        customMenu = root.findViewById(R.id.customButton);
        customMenu.setOnClickListener(this);
        relativeLayout = root.findViewById(R.id.preloading);
        chart = root.findViewById(R.id.cLine);
        initChart();
        loadData();
        return root;
    }

    private void initChart(){
        chart.getDescription().setEnabled(false);

        // enable touch gestures
        chart.setTouchEnabled(true);

        chart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);
        chart.setHighlightPerDragEnabled(true);
        chart.setPinchZoom(true);

        // set an alternative background color
        chart.setBackgroundColor(Color.WHITE);
        chart.setViewPortOffsets(0f, 0f, 0f, 0f);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setYEntrySpace(2.5f);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
//        xAxis.setTypeface(tfLight);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(true);
        xAxis.setTextColor(Color.rgb(255, 192, 56));
//        xAxis.setCenterAxisLabels(true);
        xAxis.setGranularity(1f); // one hour
        xAxis.setSpaceMin(2.5f);
        xAxis.setSpaceMax(2.5f);
        xAxis.enableGridDashedLine(1.5f, 2f, 0f);
        xAxis.setValueFormatter(new ValueFormatter() {

            private final SimpleDateFormat mFormat = new SimpleDateFormat("dd/MM", Locale.ENGLISH);

            @Override
            public String getFormattedValue(float value) {

                long millis = TimeUnit.DAYS.toMillis((long) value);
                return mFormat.format(new Date(millis));
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.enableGridDashedLine(1.5f, 2f, 0f);
        leftAxis.setTextColor(Color.rgb(255, 192, 56));

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private final int[] colors = new int[] {
            ColorTemplate.VORDIPLOM_COLORS[0],
            ColorTemplate.VORDIPLOM_COLORS[1],
            ColorTemplate.VORDIPLOM_COLORS[2]
    };

    private void setChartData(List<ChartReport> chartReportList){

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        ArrayList<Entry> confirmEntries = new ArrayList<>();
        ArrayList<Entry> recoveredEntries = new ArrayList<>();
        ArrayList<Entry> deathEntries = new ArrayList<>();

        long startDate = TimeUnit.MILLISECONDS.toDays(chartReportList.get(0).getChartReportData().getReportDate());


        for (int index = 0; index < chartReportList.size(); index++){// x = start; x < to; x++) {

            long pointDate = startDate + index;

            long totalConfirm = chartReportList.get(index).getChartReportData().getWorldConfirmed();
            long totalRecovered = chartReportList.get(index).getChartReportData().getWorldRecovered();
            long totalDeath = chartReportList.get(index).getChartReportData().getWorldDeath();

            confirmEntries.add(new Entry(pointDate, totalConfirm));
            recoveredEntries.add(new Entry(pointDate, totalRecovered));
            deathEntries.add(new Entry(pointDate, totalDeath));

        }

        LineDataSet confirmLine = new LineDataSet(confirmEntries, getString(R.string.line_label_confirmed));
        LineDataSet recoveredLine = new LineDataSet(recoveredEntries, getString(R.string.line_label_recovered));
        LineDataSet deathLine = new LineDataSet(deathEntries, getString(R.string.line_label_death));

        confirmLine.setLineWidth(2.5f);
        confirmLine.setCircleRadius(4f);
        confirmLine.setColor(Color.RED);
        confirmLine.setCircleColor(Color.RED);

        recoveredLine.setLineWidth(2.5f);
        recoveredLine.setCircleRadius(4f);
        recoveredLine.setColor(Color.GREEN);
        recoveredLine.setCircleColor(Color.GREEN);

        deathLine.setLineWidth(2.5f);
        deathLine.setCircleRadius(4f);
        deathLine.setColor(Color.BLACK);
        deathLine.setCircleColor(Color.BLACK);

        dataSets.add(confirmLine);
        dataSets.add(recoveredLine);
        dataSets.add(deathLine);

        LineData data = new LineData(dataSets);
        chart.setData(data);

        setEnableValue(enableValue);
        setEnableCircle(enableCircle);
        if (enableCubic){
            setEnableCubic();
        } else {
            setEnableStepped();
        }

        chart.invalidate();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    boolean enableValue = true,
            enableCircle = true,
            enableCubic = true;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("enableValue", enableValue);
        outState.putBoolean("enableCircle", enableCircle);
        outState.putBoolean("enableCubic", enableCubic);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            enableValue = savedInstanceState.getBoolean("enableValue");
            enableCircle = savedInstanceState.getBoolean("enableCircle");
            enableCubic = savedInstanceState.getBoolean("enableCubic");
        }

    }

    private void setEnableValue(boolean enableValue){
        if (chart != null && chart.getData() != null) {

            List<ILineDataSet> sets = chart.getData()
                    .getDataSets();

            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                set.setDrawValues(enableValue);
            }

        }
    }

    private void setEnableCircle(boolean enableCircle){
        if (chart != null && chart.getData() != null) {
            List<ILineDataSet> sets = chart.getData()
                    .getDataSets();

            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                set.setDrawCircles(enableCircle);

            }
        }
    }

    private void setEnableCubic(){
        if (chart != null && chart.getData() != null) {
            List<ILineDataSet> sets = chart.getData()
                    .getDataSets();

            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                        ? LineDataSet.Mode.LINEAR
                        : LineDataSet.Mode.CUBIC_BEZIER);
            }
        }
    }

    private void setEnableStepped(){
        if (chart != null && chart.getData() != null) {
            List<ILineDataSet> sets = chart.getData()
                    .getDataSets();

            for (ILineDataSet iSet : sets) {

                LineDataSet set = (LineDataSet) iSet;
                set.setMode(set.getMode() == LineDataSet.Mode.STEPPED
                        ? LineDataSet.Mode.LINEAR
                        : LineDataSet.Mode.STEPPED);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.line, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            switch (item.getItemId()) {
                case R.id.actionToggleValues: {
                    enableValue = !enableValue;
                    setEnableValue(enableValue);
                    chart.invalidate();
                    break;
                }
                case R.id.actionToggleStepped: {

                    enableCubic = false;
                    setEnableStepped();
                    chart.invalidate();
                    break;
                }
                case R.id.actionReload: {
                    loadData();
                    break;
                }
                case R.id.actionLocale:
                    activity.showLocaleDialog();
                    break;
            }
        }
        return true;

    }

    private void loadData(){
        relativeLayout.setVisibility(View.VISIBLE);
        chartViewModel.init(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
        chartViewModel.getChartReportRepository().observe(getViewLifecycleOwner(), reportListResponse -> {
            if (reportListResponse != null && reportListResponse.getChartReportList() != null) {
                setChartData(reportListResponse.getChartReportList());
                Toast.makeText(getContext(), R.string.load_chart_ok, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), R.string.load_chart_err, Toast.LENGTH_SHORT).show();
            }
            relativeLayout.setVisibility(View.INVISIBLE);
        });
    }

    @Override
    public void onClick() {
        loadData();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.customButton) {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(ChartFragment.this.getContext(), view);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.line, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    onOptionsItemSelected(item);
//                    Toast.makeText(MapFragment.this.getContext(), "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    }

}