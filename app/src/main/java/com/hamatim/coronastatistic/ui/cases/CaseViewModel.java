package com.hamatim.coronastatistic.ui.cases;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hamatim.coronastatistic.model._case.CaseReportListResponse;
import com.hamatim.coronastatistic.networking.ReportRepository;

public class CaseViewModel extends ViewModel {

    private String time = "";

    private MutableLiveData<CaseReportListResponse> caseReportListResponseMutableLiveData;
    private ReportRepository reportRepository;

    public void init(){
//        System.out.println("init VM");
        reportRepository = ReportRepository.getInstance();
        caseReportListResponseMutableLiveData = reportRepository.getCaseReportStream();
    }

    public void refreshData(String time){
//        System.out.println("refresh VM data");
        if (this.time.equals(time)){
            return;
        }
        this.time = time;
        reportRepository.getReports(time);
    }

    public LiveData<CaseReportListResponse> getReportRepository() {
        return caseReportListResponseMutableLiveData;
    }
}