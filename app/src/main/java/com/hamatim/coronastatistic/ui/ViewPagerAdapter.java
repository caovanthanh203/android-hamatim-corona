package com.hamatim.coronastatistic.ui;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.hamatim.coronastatistic.R;
import com.hamatim.coronastatistic.ui.about.AboutFragment;
import com.hamatim.coronastatistic.ui.cases.CaseFragment;
import com.hamatim.coronastatistic.ui.chart.ChartFragment;
import com.hamatim.coronastatistic.ui.map.MapFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.title_chart_menu, R.string.title_case_menu, R.string.title_map_menu, R.string.about};

    public void removeContext() {
        this.mContext = null;
    }

    private Context mContext;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new ChartFragment();
            case 1: return new CaseFragment();
            case 2: return new MapFragment();
        }
        return new AboutFragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 4;
    }

}