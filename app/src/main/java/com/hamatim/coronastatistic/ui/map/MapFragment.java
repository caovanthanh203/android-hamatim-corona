package com.hamatim.coronastatistic.ui.map;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.hamatim.coronastatistic.MainActivity;
import com.hamatim.coronastatistic.R;
import com.hamatim.coronastatistic.model._map.MapReport;
import com.hamatim.coronastatistic.model._map.MapReportListResponse;
import com.hamatim.coronastatistic.ui.OnCustomClickListener;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.BubbleLayout;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.HeatmapLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.hamatim.coronastatistic.Constant.CLUSTER_LAYER_ENABLE_KEY;
import static com.hamatim.coronastatistic.Constant.HEAT_LAYER_ENABLE_KEY;
import static com.hamatim.coronastatistic.Constant.POINT_LAYER_ENABLE_KEY;
import static com.mapbox.mapboxsdk.style.expressions.Expression.all;
import static com.mapbox.mapboxsdk.style.expressions.Expression.concat;
import static com.mapbox.mapboxsdk.style.expressions.Expression.division;
import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.exponential;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gte;
import static com.mapbox.mapboxsdk.style.expressions.Expression.has;
import static com.mapbox.mapboxsdk.style.expressions.Expression.heatmapDensity;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.linear;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.neq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.rgb;
import static com.mapbox.mapboxsdk.style.expressions.Expression.rgba;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.expressions.Expression.sum;
import static com.mapbox.mapboxsdk.style.expressions.Expression.toNumber;
import static com.mapbox.mapboxsdk.style.expressions.Expression.zoom;
import static com.mapbox.mapboxsdk.style.layers.Property.ICON_ANCHOR_BOTTOM;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleStrokeColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleStrokeWidth;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapIntensity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapWeight;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

public class MapFragment extends Fragment implements View.OnClickListener, OnCustomClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String PROPERTY_SELECTED = "is_selected";
    private static final String PROPERTY_NEED_RECREATE_INFO = "NEED_RECREATE_INFO";
    private MapViewModel mapViewModel;
    private MapView mapView;
    private LatLng currentPosition = new LatLng(64.900932, -18.167040);
    private GeoJsonSource geoJsonSource;
    private ProgressBar progressBar;
    private static final String EARTHQUAKE_SOURCE_ID = "earthquakes";
    private static final String GEOJSON_SOURCE_ID = "earthquakes-single";
    private static final String HEATMAP_LAYER_ID = "earthquakes-heat";
    private static final String HEATMAP_LAYER_SOURCE = "earthquakes";
    private static final String CLUSTER_SINGLE_ID = "CLUSTER_SINGLE_ID";
    private static final String CLUSTER_CHILD_LAYER_ID = "CLUSTER_CHILD_LAYER_ID";
    private static final String CLUSTER_TEXT_LAYER_ID = "CLUSTER_TEXT_LAYER_ID";
    private MapboxMap mapboxMap;
    private static final String MARKER_LAYER_ID = "MARKER_LAYER_ID";
    private FeatureCollection featureCollection;
    private GeoJsonSource source, clusterSource;
    private int selected = -1;
    private RelativeLayout relativeLayout;
    private boolean isRunning = false;
    private ImageButton customMenu;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Mapbox.getInstance(getContext(), getString(R.string.access_token));
        geoJsonSource = new GeoJsonSource("source-id",
                Feature.fromGeometry(Point.fromLngLat(currentPosition.getLongitude(),
                        currentPosition.getLatitude())));
        List<Feature> symbolLayerIconFeatureList = new ArrayList<>();
        featureCollection = FeatureCollection.fromFeatures(symbolLayerIconFeatureList);
        System.out.println("On create map");

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        System.out.println("On create map fragment");
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        relativeLayout = root.findViewById(R.id.preloading);
        customMenu = root.findViewById(R.id.customButton);
        customMenu.setOnClickListener(this);
        // object or in the same activity which contains the mapview.

        mapView = root.findViewById(R.id.mbView);
        mapView.onCreate(savedInstanceState);

//        System.out.println("Init map");
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                MapFragment.this.mapboxMap = mapboxMap;
                mapboxMap.setStyle(new Style.Builder().fromUri("mapbox://styles/caovanthanh203/ck6gv6tfh00jr1inqwvebe5yu"), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
//                        loadData();
                        mapCallback();
                    }
                });

                mapboxMap.addOnMapClickListener(new MapboxMap.OnMapClickListener() {
                    @Override
                    public boolean onMapClick(@NonNull LatLng point) {
                        return handleClickIcon(mapboxMap.getProjection().toScreenLocation(point));
                    }
                });
            }
        });

        initVM();
        registerObserver();

        if (savedInstanceState == null){
            refreshData();
        }

        ((MainActivity) getActivity()).setSharedPreferencesListener(this);

        return root;
    }

    private void initVM(){
        mapViewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        mapViewModel.init();
    }

    private void registerObserver() {
//        System.out.println("Register observe");
        mapViewModel.getReportRepository().observe(getViewLifecycleOwner(), new Observer<MapReportListResponse>() {
            @Override
            public void onChanged(@Nullable MapReportListResponse mapReportListResponse) {
//                relativeLayout.setVisibility(View.VISIBLE);
//                System.out.println("On data changed");
                if (mapReportListResponse != null && mapReportListResponse.getMapReportList() != null) {
                    initData(mapReportListResponse);
                    Toast.makeText(getContext(), R.string.load_map_ok, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), R.string.load_map_err, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initData(MapReportListResponse mapReportListResponse) {
        List<Feature> symbolLayerIconFeatureList = new ArrayList<>();
        for (MapReport mapReport : mapReportListResponse.getMapReportList()) {
            Feature feature = Feature.fromGeometry(
                    Point.fromLngLat(
                            mapReport.getMapReportData().longitude,
                            mapReport.getMapReportData().latitude)
            );
            feature.addStringProperty("title", mapReport.getMapReportData().getCountry());
            feature.addStringProperty("snippet", mapReport.getMapReportData().getDesc(
                    getString(R.string.snippet_format)
            ));
            feature.addNumberProperty("confirmed", mapReport.getMapReportData().getConfirmed());
            feature.addBooleanProperty(PROPERTY_SELECTED, false);
            feature.addBooleanProperty(PROPERTY_NEED_RECREATE_INFO, true);

            ;
            symbolLayerIconFeatureList.add(feature);
        }
        featureCollection = FeatureCollection.fromFeatures(symbolLayerIconFeatureList);
        refreshSource();
    }

    private void mapCallback() {
        if (mapboxMap != null) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {

                    style.addImage("marker_icon", getResources().getDrawable(R.drawable.marker_icon));

                    clusterSource = new GeoJsonSource(
                            EARTHQUAKE_SOURCE_ID,
                            featureCollection,
                            new GeoJsonOptions()
                                    .withCluster(true)
                                    .withClusterMaxZoom(9)
                                    .withClusterRadius(50)
//                                    .withClusterProperty("single", literal("any"), get("confirmed"))
                                    .withClusterProperty("confirmed", literal("+"), get("confirmed"))
                    );

                    style.addSource(clusterSource);

                    source = new GeoJsonSource(
                            GEOJSON_SOURCE_ID,
                            featureCollection
                    );

                    style.addSource(source);
                    initLayer(style);
                }
            });
        }
    }

    private void refreshData() {
        mapViewModel.refreshData(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
    }

    /**
     * Invoked when the bitmaps have been generated from a view.
     */
    public void setImageGenResults(HashMap<String, Bitmap> imageMap) {
        final Runtime runtime = Runtime.getRuntime();
        final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
        final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
        final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;
//        System.out.println("availHeapSizeInMB" + availHeapSizeInMB);
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
                System.out.println("add set of "+imageMap.size()+" images");
                style.addImages(imageMap);
            });
        }
    }

    public void addImage(String id, Bitmap bitmap) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
//                System.out.println("add bitmap image");
                style.addImageAsync(id, bitmap);
            });
        }
    }

    public void createImageIfNotExists(String id, Feature feature) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
                if (style.getImage(id) instanceof Bitmap){
//                    System.out.println("Has bitmap with id: " + id);
                } else {
                    createInfoView(feature, style);
                }
            });
        }
    }

    /**
     * Setup a layer with Android SDK call-outs
     * <p>
     * name of the feature is used as key for the iconImage
     * </p>
     */
    private static final String CALLOUT_LAYER_ID = "CALLOUT_LAYER_ID";

    private void setUpInfoWindowLayer(@NonNull Style loadedStyle) {
        loadedStyle.addLayer(new SymbolLayer(CALLOUT_LAYER_ID, GEOJSON_SOURCE_ID)
                .withProperties(
                        /* show image with id title based on the value of the name feature property */
                        iconImage("{title}"),

                        /* set anchor of icon to bottom-left */
                        iconAnchor(ICON_ANCHOR_BOTTOM),

                        /* all info window and marker image to appear at the same time*/
                        iconAllowOverlap(true),

                        /* offset the info window to be above the marker */
                        iconOffset(new Float[]{-2f, -28f})
                )
                .withFilter(eq((get(PROPERTY_SELECTED)), literal(true))));
    }

    private boolean handleClickIcon(PointF screenPoint) {
        List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, MARKER_LAYER_ID);
        if (!features.isEmpty()) {
            String name = features.get(0).getStringProperty("title");
            List<Feature> featureList = featureCollection.features();
            if (featureList != null) {
                for (int i = 0; i < featureList.size(); i++) {
                    if (featureList.get(i).getStringProperty("title").equals(name)) {
                        if (featureList.get(i).getBooleanProperty(PROPERTY_NEED_RECREATE_INFO)){
                            createImageIfNotExists(name, featureList.get(i));
                            featureList.get(i).addBooleanProperty(PROPERTY_NEED_RECREATE_INFO, false);
                        }
                        if (featureSelectStatus(i)) {
                            selected = -1;
                            setFeatureSelectState(featureList.get(i), false);
                        } else {
                            closeAllInfoWindows();
                            selected = i;
                            setSelected(i);
                        }
                    }
                }
            }
            return true;
        } else {
            closeAllInfoWindows();
            return false;
        }
    }

    private void closeAllInfoWindows(){
        List<Feature> featureList = featureCollection.features();
        if (featureList != null) {
            if (selected > 0) {
                setFeatureSelectState(featureList.get(selected), false);
            }
        }
    }

    /**
     * Set a feature selected state.
     *
     * @param index the index of selected feature
     */
    private void setSelected(int index) {
        if (featureCollection != null && featureCollection.features() != null) {
            Feature feature = featureCollection.features().get(index);
            setFeatureSelectState(feature, true);
            refreshSource();
        }
    }

    private void createInfoView(Feature feature, Style style) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        BubbleLayout bubbleLayout = (BubbleLayout)
                inflater.inflate(R.layout.symbol_layer_info_window_layout_callout, null);
        String name = feature.getStringProperty("title");
        TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
        titleTextView.setText(name);
        String x = feature.getStringProperty("snippet");
        TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
        descriptionTextView.setText(x);
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        bubbleLayout.measure(measureSpec, measureSpec);
        float measuredWidth = bubbleLayout.getMeasuredWidth();
        bubbleLayout.setArrowPosition(measuredWidth / 2 - 5);
//        System.out.println("Debug: Gen bit map for " + feature.getStringProperty("title"));
        Bitmap bitmap = SymbolGenerator.generate(bubbleLayout);
        style.addImageAsync(name, bitmap);
    }

    @Override
    public void onClick() {
        refreshData();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.customButton) {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(MapFragment.this.getContext(), view);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.map_menu, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    onOptionsItemSelected(item);
//                    Toast.makeText(MapFragment.this.getContext(), "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    }

    /**
     * Utility class to generate Bitmaps for Symbol.
     */
    private static class SymbolGenerator {

        /**
         * Generate a Bitmap from an Android SDK View.
         *
         * @param view the View to be drawn to a Bitmap
         * @return the generated bitmap
         */
        static Bitmap generate(@NonNull View view) {
            int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            view.measure(measureSpec, measureSpec);

            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();

            view.layout(0, 0, measuredWidth, measuredHeight);
            Bitmap bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.TRANSPARENT);
            Canvas canvas = new Canvas(bitmap);
            view.draw(canvas);
            return bitmap;
        }
    }

    /**
     * Selects the state of a feature
     *
     * @param feature the feature to be selected.
     */
    private void setFeatureSelectState(Feature feature, boolean selectedState) {
        if (feature.properties() != null) {
            feature.properties().addProperty(PROPERTY_SELECTED, selectedState);
            refreshSource();
        }
    }

    /**
     * Updates the display of data on the map after the FeatureCollection has been modified
     */
    private void refreshSource() {
        if (source != null && featureCollection != null) {
            source.setGeoJson(featureCollection);
        }
        if (clusterSource != null && featureCollection != null) {
            clusterSource.setGeoJson(featureCollection);
        }
    }

    /**
     * Checks whether a Feature's boolean "selected" property is true or false
     *
     * @param index the specific Feature's index position in the FeatureCollection's list of Features.
     * @return true if "selected" is true. False if the boolean property is false.
     */
    private boolean featureSelectStatus(int index) {
        if (featureCollection == null) {
            return false;
        }
        return featureCollection.features().get(index).getBooleanProperty(PROPERTY_SELECTED);
    }

    private void initLayer(@NonNull Style loadedMapStyle) {
        if (featureCollection != null && featureCollection.features() != null) {
            addHeatmapLayer(loadedMapStyle);
            addSinglePointOfClusterLayer(loadedMapStyle, CLUSTER_SINGLE_ID, EARTHQUAKE_SOURCE_ID);
            addMergePointOfClusterLayer(loadedMapStyle, CLUSTER_CHILD_LAYER_ID, EARTHQUAKE_SOURCE_ID);
            addTitleOfClusterLayer(loadedMapStyle, CLUSTER_TEXT_LAYER_ID, EARTHQUAKE_SOURCE_ID);
            addMarkerLayer(loadedMapStyle, MARKER_LAYER_ID, GEOJSON_SOURCE_ID);
//            runSync();
            setUpInfoWindowLayer(loadedMapStyle);
            toggleLayers();
        }
    }

    private SymbolLayer createSymbolLayer(final String layerId, final String sourceId) {
        return new SymbolLayer(layerId, sourceId);
    }

    private void addMarkerLayer(@NonNull Style loadedMapStyle, final String layerId, final String sourceId) {
        Expression value = toNumber(get("confirmed"));
        loadedMapStyle.addLayer(
                createSymbolLayer(layerId, sourceId)
                        .withProperties(
                                iconImage("marker_icon"),
                                iconAllowOverlap(true),
                                iconIgnorePlacement(true),
                                iconOffset(new Float[]{0f, -9f}))
                        .withFilter(
                                all(
                                        has("confirmed"),
                                        gt(value, literal(0))
                                )
                        )
        );
    }

    private void addSinglePointOfClusterLayer(@NonNull Style loadedMapStyle, final String layerId, final String sourceId) {
        Expression value = toNumber(get("confirmed"));
        loadedMapStyle.addLayer(
                createSymbolLayer(layerId, sourceId)
                        .withProperties(
                                iconSize(
                                        division(
                                                get("confirmed"), literal(4.0f)
                                        )
                                ),
                                iconColor(
                                        interpolate(exponential(1), get("confirmed"),
                                                stop(0, ContextCompat.getColor(getContext(), R.color.md_blue_800)),
                                                stop(10, ContextCompat.getColor(getContext(), R.color.md_green_500)),
                                                stop(100, ContextCompat.getColor(getContext(), R.color.md_yellow_400)),
                                                stop(1000, ContextCompat.getColor(getContext(), R.color.md_orange_700)),
                                                stop(10000, ContextCompat.getColor(getContext(), R.color.md_red_A400)),
                                                stop(50000, ContextCompat.getColor(getContext(), R.color.md_red_900))
                                        )
                                )
                        )
                        .withFilter(
                                all(
                                        has("confirmed"),
                                        gt(value, literal(0))
                                )
                        )
        );
    }

    private void addMergePointOfClusterLayer(@NonNull Style loadedMapStyle, final String layerId, final String sourceId) {
        int[][] layers = new int[][]{
                new int[]{50000, ContextCompat.getColor(getContext(), R.color.md_red_900)},
                new int[]{10000, ContextCompat.getColor(getContext(), R.color.md_red_A400)},
                new int[]{1000, ContextCompat.getColor(getContext(), R.color.md_orange_700)},
                new int[]{100, ContextCompat.getColor(getContext(), R.color.md_yellow_400)},
                new int[]{10, ContextCompat.getColor(getContext(), R.color.md_green_500)},
                new int[]{0, ContextCompat.getColor(getContext(), R.color.md_blue_800)}
        };

        loadedMapStyle.addLayer(
                createMergePointOfClusterLayer(
                        layers[0][0], layers[0][1], layerId + 0, sourceId
                )
        );

        for (int i = 1; i < layers.length; i++) {
            loadedMapStyle.addLayer(
                    createMergePointOfClusterLayer(
                            layers[i][0], layers[i - 1][0], layers[i][1], layerId + i, sourceId
                    )
            );
        }
    }

    private CircleLayer getCircleLayer(final String layerId, final String sourceId) {
        return new CircleLayer(layerId, sourceId);
    }

    private CircleLayer createMergePointOfClusterLayer(long startValue, long endValue, int color, final String layerId, final String sourceId) {
        Expression mergeValue = toNumber(get("confirmed"));
        return getCircleLayer(layerId, sourceId)
                .withProperties(
                        circleColor(color),
                        circleRadius(18f)
                )
                .withFilter(
                        all(has("confirmed"),
                                gte(mergeValue, literal(startValue)),
                                lt(mergeValue, literal(endValue)),
                                gt(mergeValue, literal(0))
                        )
                );
    }

    private CircleLayer createMergePointOfClusterLayer(long startValue, int color, final String layerId, final String sourceId) {
        Expression mergeValue = toNumber(get("confirmed"));
        return getCircleLayer(layerId, sourceId)
                .withProperties(
                        circleColor(color),
                        circleRadius(18f)
                )
                .withFilter(
                        all(has("confirmed"),
                                gte(mergeValue, literal(startValue)),
                                gt(mergeValue, literal(0))
                        )
                );
    }

    private void addTitleOfClusterLayer(@NonNull Style loadedMapStyle, final String layerId, final String sourceId) {
        loadedMapStyle.addLayer(
                createSymbolLayer(layerId, sourceId)
                        .withProperties(
                                textField(get("confirmed")),
                                textSize(12f),
                                textColor(Color.WHITE),
                                textIgnorePlacement(true),
                                textAllowOverlap(true)
                        ).withFilter(
                        gt(get("confirmed"), literal(0))
                )
        );
    }

    private void addHeatmapLayer(@NonNull Style loadedMapStyle) {
        HeatmapLayer layer = new HeatmapLayer(HEATMAP_LAYER_ID, GEOJSON_SOURCE_ID);
        layer.setMaxZoom(9);
        layer.setSourceLayer(HEATMAP_LAYER_SOURCE);
        layer.setProperties(
                heatmapColor(
                        interpolate(
                                linear(), heatmapDensity(),
                                literal(0), rgba(33, 102, 172, 0),
                                literal(0.2), rgb(50, 50, 255),
                                literal(0.4), rgb(50, 255, 255),
                                literal(0.6), rgb(50, 255, 50),
                                literal(0.8), rgb(255, 255, 50),
                                literal(1), rgb(255, 50, 50)
                        )
                ),
                heatmapWeight(
                        interpolate(
                                linear(), get("confirmed"),
                                stop(0, 0),
                                stop(1, 0.5),
                                stop(5000, 1),
                                stop(10000, 2)
                        )
                ),
                heatmapIntensity(
                        interpolate(
                                linear(), zoom(),
                                stop(0, 0.5),
                                stop(9, 2)
                        )
                ),
                heatmapRadius(
                        interpolate(
                                linear(), get("confirmed"),
                                stop(0, 0),
                                stop(1, 10),
                                stop(10, 20),
                                stop(100, 30),
                                stop(1000, 40),
                                stop(5000, 50),
                                stop(50000, 60)
                        )
                ),
                heatmapOpacity(
                        interpolate(
                                linear(), zoom(),
                                stop(0, 1),
                                stop(9, 0)
                        )
                )
        );

        loadedMapStyle.addLayerAbove(layer, "waterway-label");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.map_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println(item.getTitle());
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            switch (item.getItemId()) {
                case R.id.actionReload: {
                    refreshData();
                    break;
                }
                case R.id.actionHeatMap: {
                    activity.putBooleanValue(HEAT_LAYER_ENABLE_KEY, !activity.getBooleanValue(HEAT_LAYER_ENABLE_KEY));
                    break;
                }
                case R.id.actionClusterMap: {
                    activity.putBooleanValue(CLUSTER_LAYER_ENABLE_KEY, !activity.getBooleanValue(CLUSTER_LAYER_ENABLE_KEY));
                    break;
                }
                case R.id.actionMarkerMap: {
                    closeAllInfoWindows();
                    activity.putBooleanValue(POINT_LAYER_ENABLE_KEY, !activity.getBooleanValue(POINT_LAYER_ENABLE_KEY, false));
                    break;
                }
                case R.id.actionLocale:
                    activity.showLocaleDialog();
                    break;
            }
        }
        return true;
    }

    private void toggleLayer(String layerid, boolean enable) {
        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                Layer layer = style.getLayer(layerid);
                if (layer != null) {
                    if (!enable) {
                        layer.setProperties(visibility(NONE));
                    } else {
                        layer.setProperties(visibility(VISIBLE));
                    }
                }
            }
        });
    }

    private void toggleCluster(boolean enable){
        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                Layer single = style.getLayer(CLUSTER_SINGLE_ID);
                Layer cluster = style.getLayer(CLUSTER_CHILD_LAYER_ID);
                Layer text = style.getLayer(CLUSTER_TEXT_LAYER_ID);
                if (single != null) {
                    if (!enable) {
                        setLayer(single, NONE);
                        setLayer(cluster, NONE);
                        setLayer(text, NONE);
                        for (int i = 0; i < 6; i++) {
                            Layer child = style.getLayer(CLUSTER_CHILD_LAYER_ID + i);
                            setLayer(child, NONE);
                        }
                    } else {
                        setLayer(single, VISIBLE);
                        setLayer(cluster, VISIBLE);
                        setLayer(text, VISIBLE);
                        for (int i = 0; i < 6; i++) {
                            Layer child = style.getLayer(CLUSTER_CHILD_LAYER_ID + i);
                            setLayer(child, VISIBLE);
                        }
                    }
                }
            }
        });
    }

    private void setLayer(Layer layer, String status){
        if (layer != null){
            layer.setProperties(visibility(status));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            switch (key) {
                case HEAT_LAYER_ENABLE_KEY:
                    toggleLayer(HEATMAP_LAYER_ID, activity.getBooleanValue(HEAT_LAYER_ENABLE_KEY));
                    break;
                case CLUSTER_LAYER_ENABLE_KEY:
                    toggleCluster(activity.getBooleanValue(CLUSTER_LAYER_ENABLE_KEY));
                    break;
                case POINT_LAYER_ENABLE_KEY:
                    toggleLayer(MARKER_LAYER_ID, activity.getBooleanValue(POINT_LAYER_ENABLE_KEY, false));
                    break;
            }
        }
    }

    private void toggleLayers(){
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            toggleLayer(HEATMAP_LAYER_ID, activity.getBooleanValue(HEAT_LAYER_ENABLE_KEY));
            toggleLayer(MARKER_LAYER_ID, activity.getBooleanValue(POINT_LAYER_ENABLE_KEY, false));
            toggleCluster(activity.getBooleanValue(CLUSTER_LAYER_ENABLE_KEY));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) getActivity()).removeSharedPreferencesListener(this);
    }
}