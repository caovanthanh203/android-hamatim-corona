package com.hamatim.coronastatistic.ui.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.hamatim.coronastatistic.BuildConfig;
import com.hamatim.coronastatistic.R;
import com.hamatim.coronastatistic.ui.OnCustomClickListener;

public class AboutFragment extends Fragment implements OnCustomClickListener {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about, container, false);
        Button btPatreon, btKofi;
        btPatreon = v.findViewById(R.id.btPatreon);
        btKofi = v.findViewById(R.id.btKofi);
        TextView tvCallToAction = v.findViewById(R.id.tvCallToAction);
        TextView tvVersionName = v.findViewById(R.id.tvVersionName);
        tvVersionName.setText(BuildConfig.VERSION_NAME);
        tvCallToAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://apkpure.com/p/com.hamatim.coronastatistic"));
                try {
                    startActivity(browserIntent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    Toast.makeText(getContext(), R.string.open_link_err, Toast.LENGTH_SHORT).show();
                }
            }
        });
        btPatreon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.patreon.com/join/hamatimdotcom"));
                try {
                    startActivity(browserIntent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    Toast.makeText(getContext(), R.string.open_link_err, Toast.LENGTH_SHORT).show();
                }
            }
        });
        btKofi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ko-fi.com/hamatimdotcom"));
                try {
                    startActivity(browserIntent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    Toast.makeText(getContext(), R.string.open_link_err, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    @Override
    public void onClick() {
        if (hasOptionsMenu()){
            setMenuVisibility(!isMenuVisible());
        }
    }

}
