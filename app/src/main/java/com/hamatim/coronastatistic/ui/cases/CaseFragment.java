package com.hamatim.coronastatistic.ui.cases;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hamatim.coronastatistic.ConfirmComparator;
import com.hamatim.coronastatistic.HeaderAdapter;
import com.hamatim.coronastatistic.MainActivity;
import com.hamatim.coronastatistic.MarkComparator;
import com.hamatim.coronastatistic.NameComparator;
import com.hamatim.coronastatistic.OnCaseListClickListener;
import com.hamatim.coronastatistic.R;
import com.hamatim.coronastatistic.model._case.CaseReport;
import com.hamatim.coronastatistic.model._case.CaseReportListResponse;
import com.hamatim.coronastatistic.ui.OnCustomClickListener;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.hamatim.coronastatistic.Constant.CONFIRM_SORT_DIRECTION_KEY;
import static com.hamatim.coronastatistic.Constant.NAME_SORT_DIRECTION_KEY;
import static com.hamatim.coronastatistic.Constant.PIN_KEY;
import static com.hamatim.coronastatistic.Constant.TOPIC_KEY;

public class CaseFragment extends Fragment implements View.OnClickListener, OnCustomClickListener, OnCaseListClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    //    private TableLayout tableLayout;
    private RelativeLayout relativeLayout;
    private CaseViewModel caseViewModel;
    private TextView totalInfected, totalRecovered, totalDeath, totalCountry, deathRate;
    private RecyclerView recyclerView;
    private boolean isRunning = false;
    private LinearLayout linearLayout;

    private HeaderAdapter headerAdapter;
    private Observer observer;
    private ImageButton customMenu;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        System.out.println("On onCreateView");
        View root = inflater.inflate(R.layout.fragment_case, container, false);
        relativeLayout = root.findViewById(R.id.preloading);
        customMenu = root.findViewById(R.id.customButton);
        customMenu.setOnClickListener(this);
//        tableLayout = root.findViewById(R.id.tbCase);
        linearLayout = root.findViewById(R.id.customMenu);
        totalInfected = root.findViewById(R.id.tvInfected);
        totalRecovered = root.findViewById(R.id.tvRecovered);
        totalDeath = root.findViewById(R.id.tvDeath);
        totalCountry = root.findViewById(R.id.tvTotalCountry);
        deathRate = root.findViewById(R.id.tvDeathRate);
//        deathRate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getContext(), "Show menu", Toast.LENGTH_SHORT).show();
//                //getActivity().getActionBar();
//                getParentFragment()
//            }
//        });
        recyclerView = root.findViewById(R.id.rcvCase);
        headerAdapter = new HeaderAdapter();
        RecyclerView recyclerView = root.findViewById(R.id.rcvCase);
        if (headerAdapter == null) {
            headerAdapter = new HeaderAdapter();
        }
//        System.out.println("Header instance " + headerAdapter.hashCode());
        headerAdapter.setTimeFormat(
                getString(R.string.day_format),
                getString(R.string.hour_format),
                getString(R.string.min_format),
                getString(R.string.sec_format)
        );
        headerAdapter.setEnableNotification(checkPlayServices());
        headerAdapter.setOnCaseListClickListener(this);
        RecyclerView.LayoutManager recyce = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(recyce);
        recyclerView.setAdapter(headerAdapter);

        initVM();
        registerObserver();

//        System.out.println("VM instance " + caseViewModel.hashCode());

        if (savedInstanceState == null){
            refreshData();
        }

        ((MainActivity) getActivity()).setSharedPreferencesListener(this);
        return root;
    }

    private void initVM() {
        caseViewModel = ViewModelProviders.of(this).get(CaseViewModel.class);
        caseViewModel.init();
    }

    private void refreshData(){
        caseViewModel.refreshData(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
    }

    private void registerObserver() {
        observer = new Observer<CaseReportListResponse>() {
            @Override
            public void onChanged(CaseReportListResponse caseReportListResponse) {
                long totalConfirmeds = 0;
                long totalRecovereds = 0;
                long totalDeaths = 0;
                long totalCountries = 0;
                long deathRates = 0;
                if (caseReportListResponse != null && caseReportListResponse.getCaseReportList() != null) {
                    ((MainActivity) getActivity()).checkForUpdate(caseReportListResponse.getVersion(), caseReportListResponse.getUrl());
                    List<CaseReport> caseReportList = caseReportListResponse.getCaseReportList();
                    totalCountries = caseReportList.size();
                    for (CaseReport caseReport : caseReportList) {
                        totalConfirmeds += caseReport.getCaseReportData().getConfirmedLong();
                        totalRecovereds += caseReport.getCaseReportData().getRecoveredLong();
                        totalDeaths += caseReport.getCaseReportData().getDeathsLong();
//                                tableLayout.addView(tableRow);
                    }
                    headerAdapter.setData(caseReportList);
                    doSort();
                    deathRates = totalDeaths * 1000 / totalConfirmeds;
                    Toast.makeText(getContext(), R.string.load_case_ok, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), R.string.load_case_err, Toast.LENGTH_SHORT).show();
                }
                deathRate.setText(String.format("%d/1000", deathRates));
                totalCountry.setText(String.valueOf(totalCountries));
                totalInfected.setText(String.valueOf(totalConfirmeds));
                totalRecovered.setText(String.valueOf(totalRecovereds));
                totalDeath.setText(String.valueOf(totalDeaths));
                relativeLayout.setVisibility(View.INVISIBLE);
            }
        };
        caseViewModel.getReportRepository().observe(getViewLifecycleOwner(), observer);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        System.out.println("On onCreate");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.case_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            switch (item.getItemId()) {
                case R.id.actionReload: {
//                    Animation animation = new RotateAnimation(0.0f, 360.0f,
//                            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                            0.5f);
//                    animation.setDuration(2000);
//                    item.getActionView().startAnimation(animation);
                    refreshData();
                    break;
                }
                case R.id.actionToggleName: {
//                    System.out.println("Shared name" + activity.getIntValue(NAME_SORT_DIRECTION_KEY));
                    activity.putIntValue(CONFIRM_SORT_DIRECTION_KEY, 0);
                    if (activity.getIntValue(NAME_SORT_DIRECTION_KEY) == 0) {
                        activity.putIntValue(NAME_SORT_DIRECTION_KEY, 1);
                    } else {
                        activity.putIntValue(NAME_SORT_DIRECTION_KEY, -1 * activity.getIntValue(NAME_SORT_DIRECTION_KEY));
                    }
                    break;
                }
                case R.id.actionToggleConfirm: {
//                    System.out.println("Shared confirm" + activity.getIntValue(NAME_SORT_DIRECTION_KEY));
                    activity.putIntValue(NAME_SORT_DIRECTION_KEY, 0);
                    if (activity.getIntValue(CONFIRM_SORT_DIRECTION_KEY) == 0) {
                        activity.putIntValue(CONFIRM_SORT_DIRECTION_KEY, 1);
                    } else {
                        activity.putIntValue(CONFIRM_SORT_DIRECTION_KEY, -1 * activity.getIntValue(CONFIRM_SORT_DIRECTION_KEY));
                    }
                    break;
                }
                case R.id.actionLocale:
                    activity.showLocaleDialog();
                    break;
            }
        }
        return true;
    }

    @Override
    public void onSubcribeChanged(String topic) {
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            try {
                if (activity.valueInStringSet(TOPIC_KEY, topic)) {
                    Set<String> topics = activity.removeFromStringSet(TOPIC_KEY, topic);
                    activity.putStringSet(TOPIC_KEY, topics);
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
                } else {
                    Set<String> topics = activity.addToStringSet(TOPIC_KEY, topic);
                    activity.putStringSet(TOPIC_KEY, topics);
                    FirebaseMessaging.getInstance().subscribeToTopic(topic);
                }
            } catch (Exception ignored) {
            }
        }
    }

    @Override
    public void onPinChanged(String topic) {
        MainActivity activity = ((MainActivity) getActivity());
      if (activity != null) {
          if (activity.valueInStringSet(PIN_KEY, topic)) {
              Set<String> topics = activity.removeFromStringSet(PIN_KEY, topic);
              activity.putStringSet(PIN_KEY, topics);
          } else {
              Set<String> topics = activity.addToStringSet(PIN_KEY, topic);
              activity.putStringSet(PIN_KEY, topics);
          }
      }
    }

    private void doSort() {
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            int name_direction = activity.getIntValue(NAME_SORT_DIRECTION_KEY);
            int confirm_direction = activity.getIntValue(CONFIRM_SORT_DIRECTION_KEY);
            NameComparator nameComparator = new NameComparator();
            ConfirmComparator confirmComparator = new ConfirmComparator();
            MarkComparator markComparator = new MarkComparator();
            nameComparator.setDirect(name_direction);
            confirmComparator.setDirect(confirm_direction);
            if (headerAdapter != null) {
                headerAdapter.setTopics(activity.getStringSet(TOPIC_KEY));
                headerAdapter.setPins(activity.getStringSet(PIN_KEY));
                headerAdapter.reloadPin();
//                System.out.println("Name : " + name_direction);
//                System.out.println("Confirm : " + confirm_direction);
                headerAdapter.doSort(nameComparator);
                headerAdapter.doSort(confirmComparator);
                headerAdapter.doSort(markComparator);
                headerAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        MainActivity activity = ((MainActivity) getActivity());
        if (activity != null) {
            switch (key) {
                case TOPIC_KEY:
                    headerAdapter.setTopics(activity.getStringSet(TOPIC_KEY));
                    headerAdapter.notifyDataSetChanged();
                    break;
                case NAME_SORT_DIRECTION_KEY:
                case CONFIRM_SORT_DIRECTION_KEY:
                case PIN_KEY:
                    doSort();
                    break;
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        System.out.println("On destroy view");
        caseViewModel.getReportRepository().removeObserver(observer);
//        System.out.println("VM observe " + caseViewModel.getReportRepository().hasObservers());
        caseViewModel = null;
        observer = null;
        ((MainActivity) getActivity()).removeSharedPreferencesListener(this);
        headerAdapter.setOnCaseListClickListener(null);
        relativeLayout = null;
        totalInfected = null;
        totalRecovered = null;
        totalDeath = null;
        totalCountry = null;
        deathRate =  null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        System.out.println("On onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        headerAdapter = null;
//        System.out.println("On destroy");
    }

    @Override
    public void onStop() {
        super.onStop();
//        System.out.println("On onStop");
    }

    @Override
    public void onPause() {
        super.onPause();
//        System.out.println("On onPause");
    }

    @Override
    public void onClick() {
        refreshData();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.customButton) {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(CaseFragment.this.getContext(), view);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.case_menu, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    onOptionsItemSelected(item);
//                    Toast.makeText(MapFragment.this.getContext(), "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            popup.show();//showing popup menu
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

}