package com.hamatim.coronastatistic.ui.map;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hamatim.coronastatistic.model._map.MapReportListResponse;
import com.hamatim.coronastatistic.networking.ReportRepository;

public class MapViewModel extends ViewModel {

    private MutableLiveData<MapReportListResponse> mapReportListResponseMutableLiveData;
    private ReportRepository reportRepository;
    private String time = "";

    public void init(){
//        System.out.println("init VM");
        reportRepository = ReportRepository.getInstance();
        mapReportListResponseMutableLiveData = reportRepository.getMapReportStream();
    }

    public void refreshData(String time){
//        System.out.println("refresh VM data");
        if (this.time.equals(time)){
            return;
        }
        this.time = time;
        reportRepository.getMapReports(time);
    }

    public LiveData<MapReportListResponse> getReportRepository() {
        return mapReportListResponseMutableLiveData;
    }

}