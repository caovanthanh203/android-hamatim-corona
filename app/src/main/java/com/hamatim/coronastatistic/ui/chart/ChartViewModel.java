package com.hamatim.coronastatistic.ui.chart;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hamatim.coronastatistic.model._chart.ChartReportListResponse;
import com.hamatim.coronastatistic.networking.ReportRepository;

public class ChartViewModel extends ViewModel {

    private MutableLiveData<ChartReportListResponse> chartReportListResponseMutableLiveData;
    private ReportRepository reportRepository;
    private String time = "";

    public void init(String time){
        if (this.time.equals(time)){
            return;
        }
        this.time = time;
        reportRepository = ReportRepository.getInstance();
        chartReportListResponseMutableLiveData = reportRepository.getChartReports(time);

    }

    public LiveData<ChartReportListResponse> getChartReportRepository() {
        return chartReportListResponseMutableLiveData;
    }

}