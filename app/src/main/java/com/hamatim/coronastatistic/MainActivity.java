package com.hamatim.coronastatistic;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hamatim.coronastatistic.dialog.LocaleDialog;
import com.hamatim.coronastatistic.ui.NoSwipeViewPager;
import com.hamatim.coronastatistic.ui.OnCustomClickListener;
import com.hamatim.coronastatistic.ui.ViewPagerAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import java.util.HashSet;
import java.util.Set;

import static android.view.KeyEvent.KEYCODE_MENU;
import static com.hamatim.coronastatistic.Constant.THEME_LOCALE_DEFAULT_VALUE;
import static com.hamatim.coronastatistic.Constant.THEME_LOCALE_PREFERENCE_KEY;

public abstract class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, OnLocaleDialogConfirmClick, View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private NoSwipeViewPager viewPager;
    private Menu menu;
    BottomNavigationView navView;
    TextView tvAlertOldVersion;
    String url = "";
    ImageView itemChart, itemDashboard, itemMap, itemHelp, prevItem, itemReload;
    private SharedPreferences sharedPreferences;
    ViewPagerAdapter viewPagerAdapter;
    LinearLayout customMenu;
    Toolbar toolbar;
    private LocaleDialog localeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LocaleManager(this).applyLocale();
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        {
            itemChart = findViewById(R.id.itemChart);
            itemDashboard = findViewById(R.id.itemDashboard);
            itemMap = findViewById(R.id.itemMap);
            itemHelp = findViewById(R.id.itemHelp);
            itemReload = findViewById(R.id.itemReload);
//            itemFullScreen = findViewById(R.id.itemFullScreen);
        }

        customMenu = findViewById(R.id.customMenu);

        tvAlertOldVersion = findViewById(R.id.tvAlertOldVersion);
        tvAlertOldVersion.setOnClickListener(this);
        navView = findViewById(R.id.nav_view);
        viewPagerAdapter = new ViewPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        navView.setOnNavigationItemSelectedListener(this);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setCurrentItem(1);

        {
            itemChart.setOnClickListener(this);
            itemDashboard.setOnClickListener(this);
            itemMap.setOnClickListener(this);
            itemHelp.setOnClickListener(this);
            itemReload.setOnClickListener(this);
//            itemFullScreen.setOnClickListener(this);
        }

        setSharedPreferencesListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                viewPager.setCurrentItem(0);
                prevItem = itemChart;
                break;
            case R.id.navigation_dashboard:
                viewPager.setCurrentItem(1);
                prevItem = itemDashboard;
                break;
            case R.id.navigation_notifications:
                viewPager.setCurrentItem(2);
                prevItem = itemMap;
                break;
            case R.id.navigation_about:
                viewPager.setCurrentItem(3);
                prevItem = itemHelp;
                break;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (prevItem != null) {
            clearSelected(prevItem);
        }
        switch (position) {
            case 0:
                setSelected(itemChart);
                prevItem = itemChart;
                break;
            case 1:
                setSelected(itemDashboard);
                prevItem = itemDashboard;
                break;
            case 2:
                setSelected(itemMap);
                prevItem = itemMap;
                break;
            case 3:
                setSelected(itemHelp);
                prevItem = itemHelp;
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void checkForUpdate(int remoteVersion, String url) {
        this.url = url;
        if (remoteVersion > BuildConfig.VERSION_CODE) {
            tvAlertOldVersion.setVisibility(View.VISIBLE);
        } else {
            tvAlertOldVersion.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (url.equals("")) {
            url = "https://apkpure.com/p/com.hamatim.coronastatistic";
        }
        switch (view.getId()) {
            case R.id.tvAlertOldVersion: {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                try {
                    startActivity(browserIntent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    Toast.makeText(this, R.string.open_link_err, Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case R.id.itemChart:
                viewPager.setCurrentItem(0);
                break;
            case R.id.itemDashboard:
                viewPager.setCurrentItem(1);
                break;
            case R.id.itemMap:
                viewPager.setCurrentItem(2);
                break;
            case R.id.itemHelp:
                viewPager.setCurrentItem(3);
                break;
            case R.id.itemReload:
//                showLocaleDialog();
                Animation animation = new RotateAnimation(0.0f, 360.0f,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                        0.5f);
                animation.setDuration(2000);
                itemReload.startAnimation(animation);
                ((OnCustomClickListener) getSupportFragmentManager().getFragments().get(viewPager.getCurrentItem())).onClick();
                break;
        }
    }

    public SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        }
        return sharedPreferences;
    }

    public void setSharedPreferencesListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
    }

    public void removeSharedPreferencesListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
    }

    public SharedPreferences.Editor getSharedPreferencesEditor() {
        return getSharedPreferences().edit();
    }

    public Set<String> getStringSet(String key) {
        return getSharedPreferences().getStringSet(key, new HashSet<>());
    }

    public Integer getIntValue(String key) {
        return getSharedPreferences().getInt(key, 1);
    }

    public Boolean getBooleanValue(String key) {
        return getSharedPreferences().getBoolean(key, true);
    }

    public Boolean getBooleanValue(String key, boolean value) {
        return getSharedPreferences().getBoolean(key, value);
    }

    public boolean valueInStringSet(String key, String value) {
        Set<String> strings = getStringSet(key);
//        System.out.println(strings);
        if (strings.contains(value)) {
            return true;
        } else {
            return false;
        }
    }

    public Set<String> addToStringSet(String key, String value) {
        Set<String> strings = getStringSet(key);
        strings.add(value);
//        System.out.println(strings);
        return strings;
    }

    public Set<String> removeFromStringSet(String key, String value) {
        Set<String> strings = getStringSet(key);
        strings.remove(value);
        return strings;
    }

    public void putStringSet(String key, Set<String> set) {
//        System.out.println("Current " + set);
//        System.out.println("Put " + getStringSet("TOPIC"));
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(key);
        editor.apply();
        editor.putStringSet(key, set);
        editor.apply();
    }

    public void putIntValue(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putBooleanValue(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

//    public void setSharedPreferences(String key, boolean value) {
//        System.out.println("Put " + value);
//        SharedPreferences.Editor editor = getSharedPreferences().edit();
//        editor.putBoolean(key, value);
//        editor.apply();
//    }

    private void setSelected(ImageView view) {
        view.setColorFilter(getResources().getColor(R.color.md_blue_800));
    }

    private void clearSelected(ImageView view) {
        view.setColorFilter(getResources().getColor(R.color.gray));
    }

//    @Override
//    public void onConfigurationChanged(@NonNull Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//    }

//    public void showPopup(View v) {
//        PopupMenu popup = new PopupMenu(this, v);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.actions, popup.getMenu());
//        popup.show();
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        System.out.println("On activity destroy ");
        viewPagerAdapter.removeContext();
        viewPagerAdapter = null;
        viewPager = null;
        sharedPreferences = null;
        navView = null;
        tvAlertOldVersion = null;
        itemChart = null;
        itemDashboard = null;
        itemMap = null;
        itemHelp = null;
        prevItem = null;
        ;
    }

    public void showLocaleDialog() {
        localeDialog = new LocaleDialog();
        localeDialog.setOnLocaleDialogConfirmClick(this);
        localeDialog.setCurrentLocaleId(getCurrentLocaleId());
        localeDialog.show(getSupportFragmentManager(), "locale_dialog");
    }

    @Override
    public void OnLocaleChange(int localeId) {
        localeDialog = null;
        putIntValue(THEME_LOCALE_PREFERENCE_KEY, localeId);
    }


    protected int getCurrentLocaleId() {
        return getSharedPreferences().getInt(THEME_LOCALE_PREFERENCE_KEY, THEME_LOCALE_DEFAULT_VALUE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(THEME_LOCALE_PREFERENCE_KEY)) {
            recreate();
        }
    }

}
