package com.hamatim.coronastatistic;

public class Constant {

    public static final String NAME_SORT_DIRECTION_KEY = "NAME_SORT_DIRECTION_KEY";
    public static final String CONFIRM_SORT_DIRECTION_KEY = "CONFIRM_SORT_DIRECTION_KEY";

    public static final String PIN_KEY = "PIN";
    public static final String TOPIC_KEY = "TOPIC";

    public static final String HEAT_LAYER_ENABLE_KEY = "HEAT_LAYER_ENABLE_KEY";
    public static final String POINT_LAYER_ENABLE_KEY = "POINT_LAYER_ENABLE_KEY";
    public static final String CLUSTER_LAYER_ENABLE_KEY = "CLUSTER_LAYER_ENABLE_KEY";

    public static final String THEME_LOCALE_PREFERENCE_KEY = "THEME_LOCALE_PREFERENCE_KEY";
    public static final int THEME_LOCALE_DEFAULT_VALUE = 5;

}
