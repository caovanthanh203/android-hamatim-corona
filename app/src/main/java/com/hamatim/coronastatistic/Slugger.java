package com.hamatim.coronastatistic;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

public class Slugger {

    private static final Pattern NONLATIN = Pattern.compile("[^A-Za-z0-9-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    public String slugify(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }

}