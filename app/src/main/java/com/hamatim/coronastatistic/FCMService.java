package com.hamatim.coronastatistic;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hamatim.coronastatistic.a.d;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class FCMService extends FirebaseMessagingService {
	public static final String FCM_PARAM = "picture";
    private static final String DEFAULT_CHANNEL_NAME = "default";
	private static final String CHANNEL_NAME = "FCM";
	private static final String CHANNEL_DESC = "Firebase Cloud Messaging";
	private int numMessages = 0;

	@Override
	public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);
		RemoteMessage.Notification notification = remoteMessage.getNotification();
		Map<String, String> data = remoteMessage.getData();
//		Log.d("FROM", remoteMessage.getFrom());
		data.put("messageId", remoteMessage.getMessageId());
		sendNotification(notification, data);
	}

	private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data) {
		Bundle bundle = new Bundle();
		bundle.putString(FCM_PARAM, data.get(FCM_PARAM));

		Intent intent = new Intent(this, d.class);
		String body = "";
		String title = "";
		int messageNum = 0;
//		intent.putExtras(bundle);
		if (data.containsKey("confirmed_change")
				&& data.containsKey("total_confirmed")
				&& data.containsKey("country_name")){
			title = String.format(getString(R.string.notification_title), data.get("confirmed_change"), data.get("country_name"));
			body = String.format(getString(R.string.notification_body), data.get("total_confirmed"));
		} else {
			title = data.get("title");
			body = data.get("body");
		}

		if (data.containsKey("message_number")){
			messageNum = Integer.valueOf(String.valueOf(data.get("message_number")));
		} else {
			messageNum = numMessages++;
		}

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_NAME)
				.setContentTitle(title)
				.setContentText(body)
				.setAutoCancel(true)
				.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				//.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
				.setContentIntent(pendingIntent)
//				.setContentInfo("Hello")
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
                .setColor(Color.rgb(0, 198, 155))
				.setLights(Color.RED, 1000, 300)
				.setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.ic_notification)
                .setNumber(numMessages);
//		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			notificationBuilder.setSmallIcon(R.drawable.ic_launcher_foreground);
//			notificationBuilder.setColor(Color.WHITE);
//		} else {
//			notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
//		}
		try {
			String picture = data.get(FCM_PARAM);
			if (picture != null && !"".equals(picture)) {
				URL url = new URL(picture);
				Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
				notificationBuilder.setStyle(
						new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
				);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(
                    DEFAULT_CHANNEL_NAME, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
			);
			channel.setDescription(CHANNEL_DESC);
			channel.setShowBadge(true);
			channel.canShowBadge();
			channel.enableLights(true);
			channel.setLightColor(Color.RED);
			channel.enableVibration(true);
			channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

			assert notificationManager != null;
			notificationManager.createNotificationChannel(channel);
		}

		assert notificationManager != null;
//		numMessages = numMessages + 1;
//		System.out.println("Message id: " + numMessages);
		notificationManager.notify(messageNum, notificationBuilder.build());
	}
}