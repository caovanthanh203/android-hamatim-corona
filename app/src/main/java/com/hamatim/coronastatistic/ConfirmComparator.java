package com.hamatim.coronastatistic;

import com.hamatim.coronastatistic.model._case.CaseReport;

import java.util.Comparator;

public class ConfirmComparator implements Comparator<CaseReport> {

    private int direct = 1;

    public void setDirect(int direct) {
        this.direct = direct;
    }

    @Override
    public int compare(CaseReport left, CaseReport right) {
        if (left.getCaseReportData().getConfirmedLong() == right.getCaseReportData().getConfirmedLong()){
            return 0;
        }
        if (left.getCaseReportData().getConfirmedLong() > right.getCaseReportData().getConfirmedLong()){
            return direct;
        }
        return -1*direct;
    }
}
