package com.hamatim.coronastatistic;

import com.hamatim.coronastatistic.model._case.CaseReport;

import java.util.Comparator;

public class NameComparator implements Comparator<CaseReport> {

    private int direct = 1;

    public void setDirect(int direct) {
        this.direct = direct;
    }

    @Override
    public int compare(CaseReport left, CaseReport right) {
        return left.getCaseReportData().getCountry().compareToIgnoreCase(right.getCaseReportData().getCountry())*direct;
    }
}
