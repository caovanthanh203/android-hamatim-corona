package com.hamatim.coronastatistic.networking;

import androidx.lifecycle.MutableLiveData;

import com.hamatim.coronastatistic.BuildConfig;
import com.hamatim.coronastatistic.model._case.CaseReportListResponse;
import com.hamatim.coronastatistic.model._chart.ChartReportListResponse;
import com.hamatim.coronastatistic.model._map.MapReportListResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportRepository {

    private static ReportRepository reportRepository;
    private MutableLiveData<MapReportListResponse> mapReportListResponse;
    private MutableLiveData<CaseReportListResponse> caseReportListResponse;
    private String version = BuildConfig.VERSION_NAME;

    public static ReportRepository getInstance(){
        if (reportRepository == null){
            reportRepository = new ReportRepository();
        }
        return reportRepository;
    }

    private ReportApi reportApi;

    public ReportRepository(){
        reportApi = RetrofitService.cteateService(ReportApi.class);
    }

    public void getReports(String time){
//        System.out.println("Start case call");
        reportApi.getCaseReports(version, time).enqueue(new Callback<CaseReportListResponse>() {
            @Override
            public void onResponse(Call<CaseReportListResponse> call,
                                   Response<CaseReportListResponse> response) {
                if (response.isSuccessful()){
                    getCaseReportStream().setValue(response.body());
                }
//                System.out.println("End case call");
            }

            @Override
            public void onFailure(Call<CaseReportListResponse> call, Throwable t) {
                getCaseReportStream().setValue(null);
            }
        });
    }

    public MutableLiveData<ChartReportListResponse> getChartReports(String time){
        MutableLiveData<ChartReportListResponse> chartListResponse = new MutableLiveData<>();
//        System.out.println("Start call");
        reportApi.getChartReports(version, time).enqueue(new Callback<ChartReportListResponse>() {
            @Override
            public void onResponse(Call<ChartReportListResponse> call,
                                   Response<ChartReportListResponse> response) {

                if (response.isSuccessful()){
                    chartListResponse.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ChartReportListResponse> call, Throwable t) {
                chartListResponse.setValue(null);
            }
        });
//        System.out.println("End call");
        return chartListResponse;
    }

    public MutableLiveData<MapReportListResponse> getMapReportStream(){
        if (mapReportListResponse == null){
            mapReportListResponse = new MutableLiveData<>();
        }
        return mapReportListResponse;
    }

    public MutableLiveData<CaseReportListResponse> getCaseReportStream(){
        if (caseReportListResponse == null){
            caseReportListResponse = new MutableLiveData<>();
        }
        return caseReportListResponse;
    }

    public void getMapReports(String time){
//        System.out.println("Start map call");
        reportApi.getMapReports(version, time).enqueue(new Callback<MapReportListResponse>() {
            @Override
            public void onResponse(Call<MapReportListResponse> call,
                                   Response<MapReportListResponse> response) {
//                System.out.println("End map call");

                if (response.isSuccessful()){
                    getMapReportStream().setValue(response.body());
                }

            }

            @Override
            public void onFailure(Call<MapReportListResponse> call, Throwable t) {
                getMapReportStream().setValue(null);
            }
        });
    }

}
