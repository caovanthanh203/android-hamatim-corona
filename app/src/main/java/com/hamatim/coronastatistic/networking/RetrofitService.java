package com.hamatim.coronastatistic.networking;

//import java.io.IOException;
//
//import okhttp3.Interceptor;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;
import java.util.Collections;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    static HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
//    // set your desired log level

//    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
//            .addInterceptor(logging) // This is used to add ApplicationInterceptor.
////            .addNetworkInterceptor(new CustomInterceptor()) //This is used to add NetworkInterceptor.
////            .cache(null)
//            .build();

    private static Retrofit retrofit = createRetrofit();

    private static Retrofit createRetrofit(){
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
                .build();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient = enableTls13OnPreLollipop(httpClient);
        httpClient.connectionSpecs(Collections.singletonList(spec));
        httpClient.addInterceptor(logging);
        return new Retrofit.Builder()
                .baseUrl("https://corona-https.hamatim.com")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static <S> S cteateService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

//    private static class CustomInterceptor implements Interceptor {
//
//        @Override
//        public Response intercept(Interceptor.Chain chain) throws IOException {
//    /*
//    chain.request() returns original request that you can work with(modify, rewrite)
//    */
//            Request request = chain.request();
//
//            // Here you can rewrite the request
//
//        /*
//    chain.proceed(request) is the call which will initiate the HTTP work. This call invokes the request and returns the response as per the request.
//        */
//            Response response = chain.proceed(request);
//
//            //Here you can rewrite/modify the response
//
//            return response;
//        }
//    }

}
