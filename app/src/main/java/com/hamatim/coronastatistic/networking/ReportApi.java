package com.hamatim.coronastatistic.networking;

import com.hamatim.coronastatistic.model._case.CaseReportListResponse;
import com.hamatim.coronastatistic.model._chart.ChartReportListResponse;
import com.hamatim.coronastatistic.model._map.MapReportListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ReportApi {

    @GET("case-report")
    Call<CaseReportListResponse> getCaseReports(@Query("version") String version, @Query("time") String time);

    @GET("chart-report")
    Call<ChartReportListResponse> getChartReports(@Query("version") String version, @Query("time") String time);

    @GET("map-report")
    Call<MapReportListResponse> getMapReports(@Query("version") String version, @Query("time") String time);

}
