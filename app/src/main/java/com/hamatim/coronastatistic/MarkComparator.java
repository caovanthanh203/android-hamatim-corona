package com.hamatim.coronastatistic;

import com.hamatim.coronastatistic.model._case.CaseReport;

import java.util.Comparator;

public class MarkComparator implements Comparator<CaseReport> {
    @Override
    public int compare(CaseReport left, CaseReport right) {
        int left_point, right_point;
        if (left.isMarked()){
            left_point = 1;
        } else {
            left_point = 0;
        }
        if (right.isMarked()){
            right_point = 1;
        } else {
            right_point = 0;
        }
        return  right_point - left_point;
    }
}
