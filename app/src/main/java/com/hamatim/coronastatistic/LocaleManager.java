package com.hamatim.coronastatistic;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import java.util.Locale;

import static com.hamatim.coronastatistic.Constant.THEME_LOCALE_DEFAULT_VALUE;
import static com.hamatim.coronastatistic.Constant.THEME_LOCALE_PREFERENCE_KEY;

public class LocaleManager {

    private SharedPreferences sharedPreferences;
    private Context context;
    private int lightTheme;
    private int darkTheme;

    public LocaleManager(Context context){
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void applyLocale(){
        int localeCode = sharedPreferences.getInt(
                THEME_LOCALE_PREFERENCE_KEY,
                THEME_LOCALE_DEFAULT_VALUE);
        Resources resources = context.getResources();
        String[] localeCodes = resources.getStringArray(R.array.locale_codes);
        String localeName = safeGetItem(localeCodes, localeCode);
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(localeName.toLowerCase()));
        } else {
            config.locale = new Locale(localeName.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }

    private String safeGetItem(String[] array, int index) {
        try {
            return array[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "en";
        }
    }

}
