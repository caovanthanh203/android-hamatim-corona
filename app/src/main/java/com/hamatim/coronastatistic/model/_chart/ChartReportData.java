package com.hamatim.coronastatistic.model._chart;

import com.google.gson.annotations.SerializedName;

public class ChartReportData {

    @SerializedName("Mainland_China")
    public long totalInChina = 0;

    @SerializedName("Other_Locations")
    public long totalInOther = 0;

    @SerializedName("Confirmed")
    public long worldConfirmed = 0;

    @SerializedName("Death")
    public long worldDeath = 0;

    @SerializedName("Recovered")
    public long worldRecovered = 0;

    @SerializedName("Report_Date")
    public long reportDate = 0;

    @SerializedName("Total_Recovered")
    public long totalRecovered = 0;

    public long getTotalInChina() {
        return totalInChina;
    }

    public void setTotalInChina(Long totalInChina) {
        this.totalInChina = totalInChina;
    }

    public long getTotalInOther() {
        return totalInOther;
    }

    public void setTotalInOther(Long totalInOther) {
        this.totalInOther = totalInOther;
    }

    public long getReportDate() {
        return reportDate;
    }

    public void setReportDate(Long reportDate) {
        this.reportDate = reportDate;
    }

    public long getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(Long totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public long getWorldConfirmed() {
        return worldConfirmed;
    }

    public void setWorldConfirmed(long worldConfirmed) {
        this.worldConfirmed = worldConfirmed;
    }

    public long getWorldDeath() {
        return worldDeath;
    }

    public void setWorldDeath(long worldDeath) {
        this.worldDeath = worldDeath;
    }

    public long getWorldRecovered() {
        return worldRecovered;
    }

    public void setWorldRecovered(long worldRecovered) {
        this.worldRecovered = worldRecovered;
    }
}
