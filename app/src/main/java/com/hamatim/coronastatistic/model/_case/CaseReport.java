package com.hamatim.coronastatistic.model._case;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaseReport {

    private boolean marked = false;

    @SerializedName("attributes")
    private CaseReportData caseReportData;

    public CaseReportData getCaseReportData() {
        return caseReportData;
    }

    public void setCaseReportData(CaseReportData caseReportData) {
        this.caseReportData = caseReportData;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }
}
