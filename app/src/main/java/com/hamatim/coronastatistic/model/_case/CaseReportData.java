package com.hamatim.coronastatistic.model._case;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

public class CaseReportData {

    @SerializedName("Country_Region")
    private String country;

    @SerializedName("Confirmed")
    private int confirmed;

    @SerializedName("Deaths")
    private int deaths;

    @SerializedName("Recovered")
    private int recovered;

    @SerializedName("Confirmed_Change")
    private int confirmedChange = 0;

    @SerializedName("Deaths_Change")
    private int deathsChange = 0;

    @SerializedName("Recovered_Change")
    private int recoveredChange = 0;

    @SerializedName("Second_From_Last_Change")
    private long seconds = -1;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getConfirmed() {
        return String.valueOf(confirmed);
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public String getDeaths() {
        return String.valueOf(deaths);
    }

    public long getDeathsLong() {
        return deaths;
    }

    public long getConfirmedLong() {
        return confirmed;
    }

    public long getRecoveredLong() {
        return recovered;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getConfirmedChange() {
        return confirmedChange;
    }

    public void setConfirmedChange(int confirmedChange) {
        this.confirmedChange = confirmedChange;
    }

    public int getDeathsChange() {
        return deathsChange;
    }

    public void setDeathsChange(int deathsChange) {
        this.deathsChange = deathsChange;
    }

    public int getRecoveredChange() {
        return recoveredChange;
    }

    public void setRecoveredChange(int recoveredChange) {
        this.recoveredChange = recoveredChange;
    }

    public String getRecovered() {
        return String.valueOf(recovered);
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }

    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    public String getChangeAtFormat(String day, String hour, String min, String sec) {
//        Random random = new Random();
        seconds = getSeconds();

        if (seconds < 0){
            return  "-";
        }

        long days = seconds/86400;
        long seconds_for_hours = seconds - days*86400;
        long hours = seconds_for_hours/3600;
        long seconds_for_minutes = seconds_for_hours - hours*3600;
        long minutes = seconds_for_minutes/60;
        long seconds_for_seconds = seconds_for_minutes - minutes*60;

        String output = "-";

        if (days > 0){
            return String.format(day, String.valueOf(days));
        }

        if (hours > 0){
            return String.format(hour, String.valueOf(hours));
        }

        if (minutes > 0){
            return String.format(min, String.valueOf(minutes));
        }

        if (seconds_for_seconds >= 0){
            return String.format(sec, String.valueOf(seconds_for_seconds));
        }

        return output;
    }

}
