package com.hamatim.coronastatistic.model._case;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CaseReportListResponse {

    @SerializedName("features")
    private List<CaseReport> caseReportList = null;

    @SerializedName("app_version_code")
    private int version = 0;

    @SerializedName("app_url")
    private String url = "";

    public List<CaseReport> getCaseReportList() {
        return caseReportList;
    }

    public void setCaseReportList(List<CaseReport> caseReportList) {
        this.caseReportList = caseReportList;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
