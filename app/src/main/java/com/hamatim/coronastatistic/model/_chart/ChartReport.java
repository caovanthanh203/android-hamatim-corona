package com.hamatim.coronastatistic.model._chart;

import com.google.gson.annotations.SerializedName;

public class ChartReport {

    @SerializedName("attributes")
    private ChartReportData chartReportData;

    public ChartReportData getChartReportData() {
        return chartReportData;
    }

    public void setChartReportData(ChartReportData chartReportData) {
        this.chartReportData = chartReportData;
    }

}
