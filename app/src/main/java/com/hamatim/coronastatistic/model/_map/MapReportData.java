package com.hamatim.coronastatistic.model._map;

import android.annotation.SuppressLint;

import com.google.gson.annotations.SerializedName;
import com.hamatim.coronastatistic.R;

public class MapReportData {

    @SerializedName("Confirmed")
    public long confirmed;

    @SerializedName("Country_Region")
    public String country;

    @SerializedName("Deaths")
    public long deaths;

    @SerializedName("Lat")
    public Double latitude;

    @SerializedName("Long_")
    public Double longitude;

    @SerializedName("Province_State")
    public String province;

    @SerializedName("Recovered")
    public long recovered;

    public long getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(long confirmed) {
        this.confirmed = confirmed;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getDeaths() {
        return deaths;
    }

    public void setDeaths(long deaths) {
        this.deaths = deaths;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public long getRecovered() {
        return recovered;
    }

    public void setRecovered(long recovered) {
        this.recovered = recovered;
    }

    public String getDesc(String format) {
        String confirmed = "?";
        String recovered = "?";
        String death = "?";
        if (getConfirmed() != -1){
            confirmed = String.valueOf(getConfirmed());
        }
        if (getDeaths() != -1){
            death = String.valueOf(getDeaths());
        }
        if (getRecovered() != -1){
            recovered = String.valueOf(getRecovered());
        }
        return String.format(format, confirmed, death, recovered);
    }
}
