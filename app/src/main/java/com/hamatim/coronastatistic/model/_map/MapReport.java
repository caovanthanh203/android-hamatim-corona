package com.hamatim.coronastatistic.model._map;

import com.google.gson.annotations.SerializedName;

public class MapReport {

    @SerializedName("attributes")
    private MapReportData mapReportData;

    public MapReportData getMapReportData() {
        return mapReportData;
    }

    public void setMapReportData(MapReportData mapReportData) {
        this.mapReportData = mapReportData;
    }

}
