package com.hamatim.coronastatistic.model._chart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChartReportListResponse {

    @SerializedName("features")
    public List<ChartReport> chartReportList = null;

    public List<ChartReport> getChartReportList() {
        return chartReportList;
    }

    public void setChartReportList(List<ChartReport> chartReportList) {
        this.chartReportList = chartReportList;
    }

}
