package com.hamatim.coronastatistic.model._map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapReportListResponse {

    @SerializedName("features")
    private List<MapReport> mapReportList = null;

    public List<MapReport> getMapReportList() {
        return mapReportList;
    }

    public void setMapReportList(List<MapReport> mapReportList) {
        this.mapReportList = mapReportList;
    }

}
