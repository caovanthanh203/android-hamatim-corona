package com.hamatim.coronastatistic;

public interface OnCaseListClickListener {

    void onSubcribeChanged(String topic);

    void onPinChanged(String topic);

}
