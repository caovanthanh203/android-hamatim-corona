package com.hamatim.coronastatistic;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Space;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hamatim.coronastatistic.model._case.CaseReport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class HeaderAdapter extends RecyclerView.Adapter<HeaderAdapter.VHItem> {
    private OnCaseListClickListener onCaseListClickListener;
    private Set<String> pins = new HashSet<>();
    private List<CaseReport> data = new ArrayList<>();
    private Set<String> topics = new HashSet<>();
    private Slugger slugger = new Slugger();
    private boolean enableNotification = true;
    private String day_format = "";
    private String hour_format = "";
    private String min_format = "";
    private String sec_format = "";

    public HeaderAdapter() {
    }

    public void setTimeFormat(String day,String hour, String min, String sec){
        this.day_format = day;
        this.hour_format = hour;
        this.min_format = min;
        this.sec_format = sec;
    }

    public void setData(List<CaseReport> data) {
        this.data = data;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    @NonNull
    @Override
    public VHItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row, parent, false);
        return new VHItem(view);
   }

    @Override
    public void onBindViewHolder(@NonNull VHItem holder, int position) {
//        Random random = new Random();
//        if (isPositionHeader(position)) {
//            ((VHItem) holder).tvCountry.setText("Country");
//            ((VHItem) holder).tvConfirmed.setText("Confirmed");
//            ((VHItem) holder).tvDeath.setText("Death");
//            ((VHItem) holder).tvRecovered.setText("Recovered");
//        } else {
//        if (holder instanceof VHItem) {
        CaseReport dataItem = getItem(position);
        long deathRates = 0;
//        long oldDeat/hRates = 0;
        long oldCases = dataItem.getCaseReportData().getConfirmedLong();
        long oldDeaths = dataItem.getCaseReportData().getDeathsLong();
//        long deathRateChange = 0;
        if (dataItem.getCaseReportData().getDeathsLong() > 0 && dataItem.getCaseReportData().getConfirmedLong() > 0) {
            deathRates = dataItem.getCaseReportData().getDeathsLong() * 1000 / dataItem.getCaseReportData().getConfirmedLong();
        }
//        if (oldDeaths > 0 && oldCases > 0){
//            oldDeathRates = oldDeaths*1000/oldCases;
//        }
//        deathRateChange = oldDeathRates - deathRates;
        holder.tvCountry.setText(dataItem.getCaseReportData().getCountry());
        holder.tvConfirmed.setText(dataItem.getCaseReportData().getConfirmed());
        holder.tvDeath.setText(dataItem.getCaseReportData().getDeaths());
        holder.tvRecovered.setText(dataItem.getCaseReportData().getRecovered());
        holder.tvUpdateAt.setText(dataItem.getCaseReportData().getChangeAtFormat(
                day_format, hour_format, min_format, sec_format
        ));
        holder.tvDeathRate.setText(String.format("%d/1000", deathRates));
        holder.tvCountryIndex.setText(String.format("#%d", position + 1));
        String topic = slugger.slugify(dataItem.getCaseReportData().getCountry());
//        System.out.println("Country: " + dataItem.getCaseReportData().getCountry() + " Slug: " + topic);
        if (enableNotification) {
            if (topics.contains(topic)) {
                holder.btSubcribe.setImageResource(R.drawable.ic_confirm_notifications_active);
            } else {
                holder.btSubcribe.setImageResource(R.drawable.ic_notifications_off_black_24dp);
            }
        } else {
            holder.btSubcribe.setVisibility(View.GONE);
        }
        if (pins.contains(topic)){
            holder.btPin.setImageResource(R.drawable.ic_star_marked);
        } else {
            holder.btPin.setImageResource(R.drawable.ic_star_border_unmarked);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private CaseReport getItem(int position) {
        return data.get(position);
    }

    public void setOnCaseListClickListener(OnCaseListClickListener onCaseListClickListener) {
        this.onCaseListClickListener = onCaseListClickListener;
    }

    public void setPins(Set<String> pin) {
        this.pins = pin;
    }

    class VHItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvCountry, tvConfirmed, tvRecovered, tvDeath, tvDeathRate, tvUpdateAt;
        TextView tvCountryIndex, tvGetPush, tvConfirmedChange, tvRecoveredChange, tvDeathChange, tvDeathRateChange;
        ImageButton btSubcribe, btPin;

        VHItem(View itemView) {
            super(itemView);
            tvCountry = itemView.findViewById(R.id.tvCountry);
            tvConfirmed = itemView.findViewById(R.id.tvConfirmed);
            tvRecovered = itemView.findViewById(R.id.tvRecovered);
            tvDeath = itemView.findViewById(R.id.tvDeath);
            tvDeathRate = itemView.findViewById(R.id.tvDeathRate);
            tvUpdateAt = itemView.findViewById(R.id.tvUpdateAt);
            tvCountryIndex = itemView.findViewById(R.id.tvIndex);
            tvGetPush = itemView.findViewById(R.id.tvSubcribe);
            btSubcribe = itemView.findViewById(R.id.btSubcribe);
            btPin = itemView.findViewById(R.id.btPin);
            btSubcribe.setOnClickListener(this);
            btPin.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onCaseListClickListener != null) {
                switch (view.getId()) {
                    case R.id.btSubcribe:
                        onCaseListClickListener.onSubcribeChanged(slugger.slugify(tvCountry.getText().toString()));
                        break;
                    case R.id.btPin:
                        onCaseListClickListener.onPinChanged(slugger.slugify(tvCountry.getText().toString()));
                        break;
                }
            }
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
//            System.out.println("Collect VHItem");
            btSubcribe.setOnClickListener(null);
            btPin.setOnClickListener(null);
            tvCountry = null;
            tvConfirmed = null;
            tvRecovered = null;
            tvDeath = null;
            tvDeathRate = null;
            tvUpdateAt = null;
            tvCountryIndex = null;
            tvGetPush = null;
            btSubcribe = null;
            btPin = null;
        }
    }

//    private String getFormat(long number){
//        if (number > 0) {
//            return String.format("+%d", number);
//        } else if (number == 0){
//            return String.format("-");
//        } else {
//            return String.format("-%d", number);
//        }
//    }

    public void reloadPin(){
//        CustomSlug slugger = new CustomSlug();
        for(CaseReport caseReport: data){
            if (pins.contains(slugger.slugify(caseReport.getCaseReportData().getCountry()))){
                data.get(data.indexOf(caseReport)).setMarked(true);
            } else {
                data.get(data.indexOf(caseReport)).setMarked(false);
            }
        }
    }

    public void doSort(Comparator<CaseReport> comparable){
        Collections.sort(data, comparable);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
//        System.out.println("Collect header adapter");
        onCaseListClickListener = null;
        pins = null;
        data = null;
        topics = null;
        slugger = null;
    }

    public void setEnableNotification(boolean enable){
        this.enableNotification = enable;
    }

}
